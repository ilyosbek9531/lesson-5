# Task 1

## Write the below code with Promises and make sure that it's working as before
![image.png](./image.png)

# Task 2
Make a request to an api 'https://jsonplaceholder.typicode.com/users' or any other api you want with two ways, using fetch with then and using async await. If it seems easy for you, you can make some styling and show the items you got from an api.
